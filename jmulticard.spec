Name: jmulticard
Version: 1.7
Release: 0%{?dist}
Summary: Smart card access abstract layer     

License: GPL-2.0-only OR EUPL-1.1
URL: https://github.com/ctt-gob-es/jmulticard
Source0: https://github.com/ctt-gob-es/jmulticard/archive/refs/tags/v%{version}.tar.gz
Source1: http://www.gnu.org/licenses/gpl-2.0.txt
Source2: https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl1.1.-licence-en_0.pdf

BuildRequires:  fdupes
BuildRequires:  maven-local
BuildRequires:  mvn(org.bouncycastle:bcpkix-jdk15on)

%description
Smart card access abstract layer written in Java

%global debug_package %{nil}

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation/HTML

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q
cp %{SOURCE1} GPL-2.0.txt
cp %{SOURCE2} EUPL-1.1.pdf
%pom_remove_dep 'com.madgag.spongycastle:core' jmulticard
%pom_change_dep 'com.madgag.spongycastle:prov:1.58.0.0' 'org.bouncycastle:bcpkix-jdk15on:1.70' jmulticard

find . -iname *.java -exec sed -ie "s/org\.spongycastle\./org.bouncycastle./g" "{}" \;

%build
%mvn_build -f

%install
%mvn_install
%fdupes -s %{buildroot}%{_javadocdir}

%files -f .mfiles
%license GPL-2.0.txt EUPL-1.1.pdf

%files javadoc -f .mfiles-javadoc
%license GPL-2.0.txt EUPL-1.1.pdf

%changelog
* Thu May 4 2023 Francisco Torres Pérez <259762-franute@users.noreply.gitlab.com>
- Initial commit