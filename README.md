# Jmulticard packaging
Jmulticard packaging sources for Fedora.

## Description
This is an attempt to package the [jmulticard](https://github.com/ctt-gob-es/jmulticard) libraries from *ctt-gob-es* for Fedora. The final goal is to package the [cliente afirma](https://github.com/ctt-gob-es/clienteafirma) tool, but first I need to make sure dependencies are packaged correctly to build it as Maven central is not always up-to-date.

## Usage
TODO

## Support
The place to go would be [ctt-gob-es](https://github.com/ctt-gob-es) upstream profile, but it's not easy to get a response from them.

## Roadmap
The main goal is to follow upstream's releases and create patches as needed following dependant libraries new versions from Fedora (for example replacing *spongycastle* with *bouncycastle*).

## Contributing
Nothing special. I'm currently running Fedora and will use *Copr* to build and publish the packages.

## Authors and acknowledgment
TODO

## License
The original library is published under the *[GPL-v2.0](http://www.gnu.org/licenses/gpl-2.0.txt)* and *[EUPL-v1.1](http://joinup.ec.europa.eu/system/files/ES/EUPL%20v.1.1%20-%20Licencia.pdf)*

## Project status
Work in progress for now.
